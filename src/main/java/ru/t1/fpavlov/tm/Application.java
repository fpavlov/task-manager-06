package ru.t1.fpavlov.tm;

import java.util.Scanner;

import static ru.t1.fpavlov.tm.constant.TerminalConst.*;
import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;

public class Application {

    public static void main(String[] args) {
        run(args);
    }

    private static void displayWelcomeText() {
        System.out.println("**WELCOME TO TASK MANAGER**\n");
    }

    private static void displayHelp() {
        System.out.format(
                "Commands: %n" +
                        "\t %s, %s - Display program version%n" +
                        "\t %s, %s - Display developer info%n" +
                        "\t %s, %s - Display list of terminal commands%n" +
                        "\t %s - Quite%n",
                CMD_VERSION,
                CMD_SHORT_VERSION,
                CMD_ABOUT,
                CMD_SHORT_ABOUT,
                CMD_HELP,
                CMD_SHORT_HELP,
                CMD_EXIT
        );

    }

    private static void displayAbout() {
        System.out.println("\tPavlov Philipp");
        System.out.println("\tfpavlov@t1-consulting.ru");
    }

    private static void displayVersion() {
        System.out.println("\t1.6.0");
    }

    private static void run(final String[] args) {
        if (areArgumentsAvailable(args)) {
            listenerArgument(args[0]);
            return;
        }

        interactiveCommandProcessing();
    }

    private static void interactiveCommandProcessing() {
        String param;
        final Scanner scanner = new Scanner(System.in);

        displayWelcomeText();

        while (true) {
            System.out.println("-- Please enter a command --");
            param = scanner.nextLine();
            listenerCommand(param);
        }
    }

    private static void listenerCommand(final String command) {
        switch (command) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_EXIT:
                quite();
                break;
            default:
                incorrectCommand();
        }
    }

    private static void listenerArgument(final String argument) {
        switch (argument) {
            case CMD_SHORT_HELP:
                displayHelp();
                break;
            case CMD_SHORT_ABOUT:
                displayAbout();
                break;
            case CMD_SHORT_VERSION:
                displayVersion();
                break;
            default:
                incorrectArgument();
        }

        System.exit(0);
    }

    private static void incorrectCommand() {
        System.out.println("\tIncorrect Command");
    }

    private static void incorrectArgument() {
        System.out.println("\tIncorrect Argument");
    }

    private static void quite() {
        System.exit(0);
    }

    private static boolean areArgumentsAvailable(final String[] args) {
        return (args != null && args.length > 0);
    }

}
